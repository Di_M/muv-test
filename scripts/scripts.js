//Hamburger events
$('body').on('click', function(event) {
    if($(event.target.parentElement).hasClass('header__hamburger')){
        $('.header__hamburger-icon').toggleClass('fa-bars');
        $('.header__hamburger-icon').toggleClass('fa-times');
        $('.menu__list').toggleClass('menu__list--show');
    } else if (!$(event.target.parentElement).hasClass('menu__item') &&
                !$(event.target).hasClass('menu__item-link-icon')) {
        $('.header__hamburger-icon').addClass('fa-bars');
        $('.header__hamburger-icon').removeClass('fa-times');
        $('.menu__list').removeClass('menu__list--show');
        $('.menu__item-droplist').removeClass('menu__item-droplist--open');
    }
});


//DropList events
if($(window).width() < 992) {
    $('.menu__item').on('click', function() {
        $(this).find('.menu__item-droplist').toggleClass('menu__item-droplist--open');
    });
}


//Languages buttons event
$('.lang__btn').on('click', function() {
    console.log("Hello");
    $(this).addClass('lang__btn-active').siblings().removeClass('lang__btn-active');
});
